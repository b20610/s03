import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        HashMap<String,Integer> gameInventory = new HashMap<>();
        gameInventory.put("Mario Odyssey",50);
        gameInventory.put("Super Smash Bros. Ultimate",20);
        gameInventory.put("Luigi's Mansion 3",15);
        gameInventory.put("Pokemon Sword",30);
        gameInventory.put("Pokemon Shield",100);

        gameInventory.forEach((game,stock) -> {
            System.out.println(game + " has " + stock + " stocks left.");
        });

        ArrayList<String> topGames = new ArrayList<>();

        gameInventory.forEach((game,stock) -> {
            if(stock <= 30){
                topGames.add(game);
                System.out.println(game + " has been added to top games list!");
            }
        });

        System.out.println("Our shop's top games:");
        System.out.println(topGames);

        boolean addItem = true;
        while(addItem){

            System.out.println("Would you like to add an item? Yes or No.");
            Scanner userInput = new Scanner(System.in);
            String input = userInput.nextLine();

            switch(input){
                case "Yes":
                    System.out.println("Add the item name:");
                    String itemName = userInput.nextLine();

                    System.out.println("Add the item stock:");
                    int itemStock = 0;
                    try{
                        itemStock = userInput.nextInt();
                        gameInventory.put(itemName,itemStock);
                        System.out.println("Updated Inventory:");
                        System.out.println(gameInventory);

                        if(itemStock <= 30){
                            topGames.add(itemName);
                            System.out.println(itemName + " has been added to top games list!");
                            System.out.println("Updated top Games:");
                            System.out.println(topGames);
                        }
                    } catch(Exception e){
                        System.out.println("Invalid input");
                    }

                    break;
                case "No":
                    addItem = false;
                    System.out.println("Thank you!");
                    break;
                default:
                    System.out.println("Please answer Yes or No only.");
            }
        }

    }
}